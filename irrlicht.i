/*
guile-irrlicht --- GNU Guile bindings for Irrlicht Engine

Copyright (C) 2021 Javier Sancho <jsf@jsancho.org>

This file is part of guile-irrlicht.

guile-irrlicht is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation; either version 3 of the
License, or (at your option) any later version.

guile-irrlicht is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with guile-irrlicht. If not, see
<http://www.gnu.org/licenses/>.
*/

%module irrlicht
%{
#include <irrlicht/irrlicht.h>
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
%}

%typecheck (SWIG_TYPECHECK_POINTER) const IRR_CHAR_TYPE &
{
  $1 = scm_is_string ($input);
}
%typemap (in) const IRR_CHAR_TYPE &
{
  $1 = ($ltype) scm_to_utf8_string ($input);
}
%typemap (freearg) const IRR_CHAR_TYPE &
{
  free ($1);
}

%typecheck (SWIG_TYPECHECK_POINTER) const IRR_WCHAR_TYPE &
{
  $1 = scm_is_string ($input);
}
%typemap (in) const IRR_WCHAR_TYPE &
{
  $1 = ($ltype) scm_to_utf32_string ($input);
}
%typemap (freearg) const IRR_WCHAR_TYPE &
{
  free ($1);
}

%typecheck (SWIG_TYPECHECK_POINTER) const IRR_IO_PATH &
{
  $1 = scm_is_string ($input);
}
%typemap (in) const IRR_IO_PATH &
{
  $1 = new $*1_ltype (scm_to_utf8_string ($input));
}
%typemap (freearg) const IRR_IO_PATH &
{
  delete $1;
}

%include irrString.h
%apply const IRR_CHAR_TYPE & { const irr::core::stringc & };
%apply const IRR_WCHAR_TYPE & { const wchar_t *, const irr::core::stringw & };
%apply const IRR_IO_PATH & { const irr::io::path &, const path &, irr::io::path &, path & };

typedef int s32;
typedef unsigned int u32;
typedef float f32;
typedef double f64;

#define _IRR_DEPRECATED_

%rename (add) operator+;
%rename (substract) operator-;
%rename (product) operator*;
%rename (divide) operator/;
%rename (equal) operator==;
%rename (not_equal) operator!=;
%rename (less) operator<;
%rename (less_equal) operator<=;
%rename (greater) operator>;
%rename (greater_equal) operator>=;
%ignore operator=;
%ignore operator+=;
%ignore operator-=;
%ignore operator*=;
%ignore operator/=;
%ignore operator!=;

%include dimension2d.h
%template (dimension2df) irr::core::dimension2d<irr::f32>;
%template (dimension2du) irr::core::dimension2d<irr::u32>;
%template (dimension2di) irr::core::dimension2d<irr::s32>;

%include rect.h
%template (rectf) irr::core::rect<irr::f32>;
%template (recti) irr::core::rect<irr::s32>;

%include vector3d.h
%template (vector3df) irr::core::vector3d<irr::f32>;
%template (vector3di) irr::core::vector3d<irr::s32>;

%rename ("%(undercase)s", %$not %$isconstant, %$not %$isenumitem) "";
%feature ("constasvar");

%include EDriverTypes.h
%include EMaterialFlags.h
%include IrrCompileConfig.h

%rename (color) irr::video::SColor;
%rename (color) irr::video::SColor::SColor;
%rename (xcolor) irr::video::SColor::color;
%rename (get_color_alpha) irr::video::SColor::getAlpha;
%rename (get_color_red) irr::video::SColor::getRed;
%rename (get_color_green) irr::video::SColor::getGreen;
%rename (get_color_blue) irr::video::SColor::getBlue;
%rename (get_color_average) irr::video::SColor::getAverage;
%rename (get_colorf_alpha) irr::video::SColorf::getAlpha;
%rename (get_colorf_red) irr::video::SColorf::getRed;
%rename (get_colorf_green) irr::video::SColorf::getGreen;
%rename (get_colorf_blue) irr::video::SColorf::getBlue;
%rename (get_colorf_average) irr::video::SColorf::getAverage;
%include SColor.h

%include IReferenceCounted.h
%include IAttributeExchangingObject.h

%include IMesh.h

%rename (animated_mesh) irr::scene::IAnimatedMesh;
%include IAnimatedMesh.h

%include IAnimatedMeshMD2.h
%include SAnimatedMesh.h

%rename (scene_node) irr::scene::ISceneNode;
%include ISceneNode.h

%rename (animated_mesh_scene_node) irr::scene::IAnimatedMeshSceneNode;
%rename (set_md2_animation) irr::scene::IAnimatedMeshSceneNode::setMD2Animation;
%include IAnimatedMeshSceneNode.h

%include IEventReceiver.h
%include IGUIElement.h

%rename (gui_static_text) irr::gui::IGUIStaticText;
%include IGUIStaticText.h

%rename (get_gui_environment) getGUIEnvironment;
%rename (gui_environment) irr::gui::IGUIEnvironment;
%include IGUIEnvironment.h

%rename (scene_manager) irr::scene::ISceneManager;
%include ISceneManager.h

%ignore irr::video::IVideoDriver::createImage;
%rename (apply_material) apply;
%rename (video_driver) irr::video::IVideoDriver;
%include IVideoDriver.h

%rename (yield_device) yield;
%rename (sleep_device) sleep;
%include IrrlichtDevice.h
%include irrlicht.h

%scheme %{ (load-extension "libguile-irrlicht" "scm_init_irrlicht_module") %}
