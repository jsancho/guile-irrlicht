;;; guile-irrlicht --- FFI bindings for Irrlicht Engine
;;; Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>
;;;
;;; This file is part of guile-irrlicht.
;;;
;;; Guile-irrlicht is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-irrlicht is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-irrlicht.  If not, see
;;; <http://www.gnu.org/licenses/>.


;;; Irrlicht 01.HelloWorld example
;;; http://irrlicht.sourceforge.net/docu/example001.html


(use-modules (irrlicht)
             (oop goops))

(define (media-path file)
  (let ((current-path (dirname (current-filename))))
    (string-join (list current-path "media" file) file-name-separator-string)))

;; start up the engine
(define device
  (create-device
   EDT-SOFTWARE
   (make <dimension2du> #:args '(640 480))))

(set-window-caption device "Hello World! - Irrlicht Engine Demo")

(define driver (get-video-driver device))
(define scene-manager (get-scene-manager device))
(define gui-env (get-gui-environment device))

;; static text
(add-static-text
 gui-env
 "Hello World! This is the Irrlicht Software renderer!"
 (make <recti> #:args '(10 10 260 22))
 #t)

;; load a Quake2 model
(define mesh (get-mesh scene-manager (media-path "sydney.md2")))
(define node (add-animated-mesh-scene-node scene-manager mesh))
(set-material-flag node EMF-LIGHTING #f)
(set-md2-animation node EMAT-STAND)
(set-material-texture node 0 (get-texture driver (media-path "sydney.bmp")))

;; place camera
(let ((position (make <vector3df> #:args '(0 30 -40)))
      (lookat (make <vector3df> #:args '(0 5 0))))
  (add-camera-scene-node scene-manager node position lookat))

;; draw everything
(let ((back-buffer #t)
      (z-buffer #t)
      (color (make <color> #:args '(255 100 101 140))))
  (while (run device)
    (begin-scene driver back-buffer z-buffer color)
    (draw-all scene-manager)
    (draw-all gui-env)
    (end-scene driver)))

;; delete device
(drop device)
(exit #t)
