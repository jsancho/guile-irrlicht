;;; guile-irrlicht --- FFI bindings for Irrlicht Engine
;;; Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>
;;;
;;; This file is part of guile-irrlicht.
;;;
;;; Guile-irrlicht is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-irrlicht is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-irrlicht.  If not, see
;;; <http://www.gnu.org/licenses/>.


;;; Irrlicht 02.Quake3Map example
;;; http://irrlicht.sourceforge.net/docu/example002.html


(use-modules (irrlicht)
             (ice-9 match))

;; ask user for driver
(format #t
        "Please select the driver you want for this example:
 (a) OpenGL 1.5
 (b) Direct3D 9.0c
 (c) Direct3D 8.1
 (d) Burning's Software Renderer
 (e) Software Renderer
 (f) NullDevice
 (otherKey) exit~%~%")

(define driver (match (read-char)
                      (#\a 'opengl)
                      (#\b 'direct3d9)
                      (#\c 'direct3d8)
                      (#\d 'burnings)
                      (#\e 'software)
                      (#\f 'null)
                      (_ #f)))

(when (not driver)
  (exit #f))

;; start up the engine
(define device
  (create-device
   #:device-type driver
   #:window-size '(640 480)))
(when (not device)
  (exit #f))

;; instances for doing things
(define driver (get-video-driver device))
(define scene-manager (get-scene-manager device))
(define driver-name (get-name driver))

;; load Quake3 map
(add-file-archive! (get-file-system device) "media/map-20kdm2.pk3")

(define mesh (get-mesh scene-manager "20kdm2.bsp"))
(define node (add-octree-scene-node!
              scene-manager mesh
              #:minimal-polys-per-node 1024))
(set-position! node '(-1300 -144 -1249))

;; FPS camera
(add-camera-scene-node-fps! scene-manager)
(set-visible! (get-cursor-control device) #f)

;; loop
(define last-fps -1)
(while (run device)
  (cond ((is-window-active? device)
         (begin-scene driver #:color '(255 200 200 200))
         (draw-all scene-manager)
         (end-scene driver)

         (let ((fps (get-fps driver)))
           (when (not (= last-fps fps))
             (let ((caption
                    (format #f "Irrlicht Engine - Quake 3 Map example [~a] FPS:~a" driver-name fps)))
               (set-window-caption! device caption))
             (set! last-fps fps))))
        (else
         (yield-device device))))

;; delete device
(drop! device)
(exit #t)
