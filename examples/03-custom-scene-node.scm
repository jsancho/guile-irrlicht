;;; guile-irrlicht --- FFI bindings for Irrlicht Engine
;;; Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>
;;;
;;; This file is part of guile-irrlicht.
;;;
;;; Guile-irrlicht is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-irrlicht is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-irrlicht.  If not, see
;;; <http://www.gnu.org/licenses/>.


;;; Irrlicht 03.CustomSceneNode example
;;; http://irrlicht.sourceforge.net/docu/example003.html


(use-modules (irrlicht)
             (ice-9 match))

;; ask user for driver
(format #t
        "Please select the driver you want for this example:
 (a) OpenGL 1.5
 (b) Direct3D 9.0c
 (c) Direct3D 8.1
 (d) Burning's Software Renderer
 (e) Software Renderer
 (f) NullDevice
 (otherKey) exit~%~%")

(define device-type (match (read-char)
                           (#\a 'opengl)
                           (#\b 'direct3d9)
                           (#\c 'direct3d8)
                           (#\d 'burnings)
                           (#\e 'software)
                           (#\f 'null)
                           (_ #f)))

(when (not device-type)
  (exit #f))

;; start up the engine
(define device
  (create-device
   #:device-type device-type
   #:window-size '(640 480)))

;; create engine and camera
(set-window-caption! device "Custom Scene Node - Irrlicht Engine Demo")

(define my-node #f)
(define driver (get-video-driver device))
(define scene-manager (get-scene-manager device))
(add-camera-scene-node! scene-manager
                        #:position '(0 -40 0)
                        #:lookat '(0 0 0))

;; create our custom scene node
(define box (make-box3d))
(define vertices
  (list (make-vertex3d '(0 0 10) '(1 1 0) '(255 0 255 255) '(0 1))
        (make-vertex3d '(10 0 -10) '(1 0 0) '(255 255 0 255) '(1 1))
        (make-vertex3d '(0 20 0) '(0 1 1) '(255 255 255 0) '(1 0))
        (make-vertex3d '(-10 0 -10) '(0 0 1) '(255 0 255 0) '(0 0))))
(define material (make-material #:wireframe #f #:lighting #f))

(reset-box3d! box (get-position (car vertices)))
(for-each
 (lambda (vertex)
   (add-internal-point! box (get-position vertex)))
 (cdr vertices))

(define (custom-render)
  (let ((indices '((0 2 3) (2 1 3) (1 0 3) (2 0 1))))
    (set-material! driver material)
    (set-transform! driver 'world (get-absolute-transformation my-node))
    (draw-vertex-primitive-list driver vertices indices)))

(define (custom-get-bounding-box)
  box)

(define (custom-get-material-count)
  1)

(define (custom-get-material i)
  material)

(set! my-node (add-custom-scene-node! scene-manager
                                      custom-render
                                      custom-get-bounding-box
                                      custom-get-material-count
                                      custom-get-material
                                      #:parent (get-root-scene-node scene-manager)))

;; add rotation
(let ((anim (create-rotation-animator scene-manager '(0.8 0 0.8))))
  (add-animator! my-node anim))

;; loop
(define frames 0)
(while (run device)
  (begin-scene driver #:color '(0 100 100 100))
  (draw-all scene-manager)
  (end-scene driver)

  (set! frames (+ frames 1))
  (when (= frames 100)
    (let ((fps (get-fps driver))
          (driver-name (get-name driver)))
      (let ((caption
             (format #f "Irrlicht Engine [~a] FPS:~a" driver-name fps)))
        (set-window-caption! device caption)))
    (set! frames 0)))

;; delete device
(drop! device)
(exit #t)
