;;; guile-irrlicht --- FFI bindings for Irrlicht Engine
;;; Copyright (C) 2020 Javier Sancho <jsf@jsancho.org>
;;;
;;; This file is part of guile-irrlicht.
;;;
;;; Guile-irrlicht is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-irrlicht is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-irrlicht.  If not, see
;;; <http://www.gnu.org/licenses/>.


;;; Irrlicht 04.Movement example
;;; http://irrlicht.sourceforge.net/docu/example004.html


(use-modules (irrlicht)
             (ice-9 match))

;; ask user for driver
(format #t
        "Please select the driver you want for this example:
 (a) OpenGL 1.5
 (b) Direct3D 9.0c
 (c) Direct3D 8.1
 (d) Burning's Software Renderer
 (e) Software Renderer
 (f) NullDevice
 (otherKey) exit~%~%")

(define device-type (match (read-char)
                           (#\a 'opengl)
                           (#\b 'direct3d9)
                           (#\c 'direct3d8)
                           (#\d 'burnings)
                           (#\e 'software)
                           (#\f 'null)
                           (_ #f)))

(when (not device-type)
  (exit #f))

;; create event receiver
(define key-is-down '())
(define (is-key-down? key-code)
  (assoc-ref key-is-down key-code))

(define (on-event event)
  (if (equal? (get-event-type event) 'key-input-event)
      (set! key-is-down
            (assoc-set! key-is-down
                        (get-event-key event)
                        (get-event-key-pressed event))))
  #f)

(define receiver (make-event-receiver on-event))

;; create device
(define device
  (create-device
   #:device-type device-type
   #:window-size '(640 480)
   #:receiver receiver))

(define driver (get-video-driver device))
(define scene-manager (get-scene-manager device))
(define gui-env (get-gui-environment device))

;; create the node which will be moved with the WSAD keys
(define ball (add-sphere-scene-node! scene-manager))
(set-position! ball '(0 0 30))
(set-material-texture! ball 0 (get-texture driver "media/wall.bmp"))
(set-material-flag! ball 'lighting #f)

;; create another node, movable using a scene node animator
(let ((cube (add-cube-scene-node! scene-manager))
      (anim (create-fly-circle-animator scene-manager #:center '(0 0 30) #:radius 20)))
  (set-material-texture! cube 0 (get-texture driver "media/t351sml.jpg"))
  (set-material-flag! cube 'lighting #f)
  (add-animator! cube anim)
  (drop! anim))

;; another scene with a b3d model and a 'fly straight' animator
(let ((ninja (add-animated-mesh-scene-node!
              scene-manager (get-mesh scene-manager "media/ninja.b3d")))
      (anim (create-fly-straight-animator
             scene-manager '(100 0 60) '(-100 0 60) 3500 #:loop #t)))
  (add-animator! ninja anim)
  (drop! anim)

  ;; make the model look right
  (set-material-flag! ninja 'lighting #f)
  (set-frame-loop! ninja 0 13)
  (set-animation-speed! ninja 15)
  (set-scale! ninja '(2 2 2))
  (set-rotation! ninja '(0 -90 0)))

;; create a first person shooter camera
(add-camera-scene-node-fps! scene-manager)
(set-visible! (get-cursor-control device) #f)

;; colorful irrlicht logo
(add-image! gui-env (get-texture driver "media/irrlichtlogoalpha2.tga") '(10 20))
(let ((diagnostics (add-static-text! gui-env "" '(10 10 400 20))))
  (set-override-color! diagnostics '(255 255 255 0)))

;; game loop
(let ((timer (get-timer device))
      (driver-name (get-name driver)))
  (let ((last-fps -1)
        (then (get-time timer))
        (movement-speed 5))
    (while (run device)
      (let* ((now (get-time timer))
             (frame-delta-time (/ (- now then) 1000)))
        (set! then now)

        ;; check if W, S, A or D are pressed
        (let* ((node-position (get-position ball))
               (pos-x (car node-position))
               (pos-y (cadr node-position))
               (pos-z (caddr node-position)))
          (if (is-key-down? 'key-w)
              (set! pos-y (+ pos-y (* movement-speed frame-delta-time))))
          (if (is-key-down? 'key-s)
              (set! pos-y (- pos-y (* movement-speed frame-delta-time))))
          (if (is-key-down? 'key-a)
              (set! pos-x (- pos-x (* movement-speed frame-delta-time))))
          (if (is-key-down? 'key-d)
              (set! pos-x (+ pos-x (* movement-speed frame-delta-time))))
          (set-position! ball (list pos-x pos-y pos-z))))

      (begin-scene driver #:color '(255 113 113 133))
      (draw-all scene-manager)
      (draw-all gui-env)
      (end-scene driver)

      (let ((fps (get-fps driver)))
        (when (not (= last-fps fps))
          (let ((caption
                 (format #f "Movement Example - Irrlicht Engine [~a] fps: ~a" driver-name fps)))
            (set-window-caption! device caption))
          (set! last-fps fps))))))

;; delete device
(drop! device)
(exit #t)
